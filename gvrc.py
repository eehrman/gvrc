from __future__ import print_function
import csv
import os
import glob
from os.path import expanduser
from os import listdir
from os.path import isfile, join
from io import StringIO
import re
# from enum import Enum
import sys
import copy

# fnt = '~/apu/libs/gnl_process_dev/libgnl_process_dev_custom.c'
# fname_in = 'custom.gvr'
# fnt_out = 'custom.c'
ver = '160919'
# g_fnt_in = ''
GVRC_PAT = 'GVRC_'

# These will soon be passable params (later: ?!?)
# The plan is to allow the following permanently allocated vrs
# 1. M1_INDEX // called M4 elsewhere
# 2. FLAGS_ROW
# 3. FLAGS_BACKUP
# For marks (flags) the permanently allocated are:
# 1. C_FLAG
# 2. PE_FLAG
# 3. PRINT_FLAG

c_max_live_marks = 11 # 16 - print,c,pe,m_starts,m_ends (total 5)
c_max_preassigned_mrks = 5
c_max_live_vrs = 15 # 24 - idx, flags, vr storage of mrks, vr_windex, vr_valid_mrks db (4) total 8
c_max_preassigned_vrs = 5
c_max_stored_marks = 16
c_max_stored_vrs = 8
c_max_rn_regs_avail = 15 # The ONLY! permanently allocated rn reg is 15, holding value (row) 23, the FLAGS row
c_b_combine_setting = True

assert c_max_rn_regs_avail <= c_max_live_vrs, 'Configuration Error. c_max_rn_regs_avail must be <= c_max_live_vrs'

c_sm_defs_tbl = [
	# b_avail, b_must_restore, restore_val
	(True, False, '0x0000'),
	(True, False, '0x0000'),
	(True, False, '0x0000'),
	(True, False, '0x0000'),
	(False, False, '0xffff'), # SM_REG_4, used a lot
	(False, True, '0X0001'), # SM_REG_5, used a lot, eg. used in count 1s, used gvrc_add_u16, gvrc_le_u16, gvrc_le_imm_u16
	(True, True, '0X1111'), # SM_REG_6, used in store and load and max_nibble, no longer used gvrc_add_u16
	(True, True, '0X0101'), #  SM_REG_7, no longer used in gvrc_le_u16, gvrc_le_imm_u16
	(True, False, '0X000F'), #  SM_REG_8, no longer used gvrc_add_u16
	(True, False, '0X0F0F'), #  SM_REG_9, used in count 1s, gvrc_le_u16, gvrc_le_imm_u16
	(True, False, '0X0707'), #  SM_REG_10, used in gvrc_le_u16, gvrc_le_imm_u16
	(True, False, '0X5555'), #  SM_REG_11, no longer used in count 1s
	(False, True, '0X3333'), #  SM_REG_12, used in load, store, count 1s, used gvrc_add_u16
	(True, False, '0X00FF'), #  SM_REG_13, no longer used in gvrc_le_u16, gvrc_le_imm_u16
	(False, True, '0X001F'), #  SM_REG_14, used in gvrc_le_u16, gvrc_le_imm_u16
	(True, True, '0X003F') #  SM_REG_15
]

cbstore = True

g_apl_hdr = '''/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

APL_C_INCLUDE <gsi/preproc_defs.h>
APL_C_INCLUDE <gsi/libapl.h>

#include "ipl-apl-funcs.apl.h"
//#include "gvrc_fused_ops.h"


// WARNING! This is an automatically generated file. Do not modify!

'''

g_gvrc_h_hdr = ''' /*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#ifndef GVRC_FUSED_OPS_H
#define GVRC_FUSED_OPS_H

//#include <gsi/common_api.h>
//#include <gsi/libapl_base.h>

// WARNING! This is an automatically generated file. Do not modify!

'''

g_gvrc_h_ftr = '#endif // GVRC_FUSED_OPS_H\n'

g_gvrc_c_hdr = ''' /*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

APL_C_INCLUDE <stddef.h>
APL_C_INCLUDE <gsi/libapl.h>
APL_C_INCLUDE <gsi/libsys/log.h>
APL_C_INCLUDE <gsi/libgal.h>

#include "../ipl-apl-funcs.apl.h"
#include "../etest-apl-funcs.apl.h"
//#include "gvrc_fused_ops.h"


APL_FRAG rsp_2k_rl_out(VOID)
{
	SM_0XFFFF: RSP16 = RL;
	RSP256 = RSP16;
	RSP2K = RSP256;
	RSP32K = RSP2K;
	NOOP;
	NOOP;
	RSP_END;
};


'''

#pragma GCC diagnostic ignored "-Wself-assign"
#pragma GCC diagnostic ignored "-Wunused-parameter"



class e_var_type(object):
	evtVR = 1
	evtMRK = 2
	evtVL1 = 3

var_opts = [(e_var_type.evtMRK, 'evtMRK'), (e_var_type.evtVR, 'evtVR'), (e_var_type.evtVL1, 'evtVL1')]

class e_used_type(object):
	eutUnused = 1
	eutSet = 2
	eutRead = 3
	eutModify = 4
	eutTemp = 5
	# eutForInfo = 5 # var needed for preparing info obj or setting as an arg in that obj. not set, read or modified in this line

class e_move_type(object):
	emtStageToStore = 1
	emtStoreToStage = 2
	emtStageToStage = 3
	emtStoreToStore = 4

VR_MRK_BACKUP = '(enum gvrc_vr16)(VR16_G_LAST)'

def output_comments_and_moves(out_data_obj, iline, info_ptr, d_map_line_to_move_cmd, d_map_line_to_cmt, ltext, ttext):
	l_comments = d_map_line_to_cmt.get(iline, [])

	for cmt in l_comments:
		# fh.write(ltext + cmt + ttext)
		out_data_obj.add_line(iline, ltext, ttext, cmt, b_no_fuse=False, fn_name=None, l_args=[])

	l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
	for move_cmd in l_move_cmds:
		iline, move_var, ifrom, ito, emt = move_cmd
		l_out_args = []
		if move_var.get_evt() == e_var_type.evtMRK:
			if emt == e_move_type.emtStageToStore:
				sf = info_ptr.get_name() + '->use_mrk[' + str(ifrom) + ']'
				sm = 'gvrc_cpy_from_mrk_16_msk(' + VR_MRK_BACKUP + ', '
				sm += '(enum gvml_mrks_n_flgs)(1 << ' + sf + '), '
				sm += '(u16)(1 << ' + str(ito) + '))'
				l_out_args.append(cl_arg_out_data(e_var_type.evtVR, -1, VR_MRK_BACKUP))
				l_out_args.append(cl_arg_out_data(e_var_type.evtMRK, ifrom, None))
				l_out_args.append(cl_arg_out_data(None, -1, '(u16)(1 << ' + str(ito) + ')'))
				out_data_obj.add_line(	iline, ltext, ttext, sm, b_no_fuse=False,
										fn_name='gvrc_cpy_from_mrk_16_msk', l_args=l_out_args)
			elif emt == e_move_type.emtStoreToStage:
				st = info_ptr.get_name() + '->use_mrk[' + str(ito) + ']'
				sm = 'gvrc_cpy_mrk_to_std('
				sm += '(enum gvml_mrks_n_flgs)(1 << ' + st + '), ' + VR_MRK_BACKUP + ', '
				sm += '(u16)(1 << ' + str(ifrom) + '))'
				l_out_args.append(cl_arg_out_data(e_var_type.evtMRK, ito, None))
				l_out_args.append(cl_arg_out_data(e_var_type.evtVR, -1, VR_MRK_BACKUP))
				l_out_args.append(cl_arg_out_data(None, -1, '(u16)(1 << ' + str(ifrom) + ')'))
				out_data_obj.add_line(	iline, ltext, ttext, sm, b_no_fuse=False,
										fn_name='gvrc_cpy_mrk_to_std', l_args=l_out_args)
				# fh.write(sm)
			elif emt == e_move_type.emtStageToStage:
				sf = info_ptr.get_name() + '->use_mrk[' + str(ifrom) + ']'
				st = info_ptr.get_name() + '->use_mrk[' + str(ito) + ']'
				sm = 'gvrc_m_cpy( '
				sm += '(enum gvml_mrks_n_flgs)(1 << ' + st + '), '
				sm += '(enum gvml_mrks_n_flgs)(1 << ' + sf + ')) '
				l_out_args.append(cl_arg_out_data(e_var_type.evtMRK, ito, None))
				l_out_args.append(cl_arg_out_data(e_var_type.evtMRK, ifrom, None))
				out_data_obj.add_line(	iline, ltext, ttext, sm, b_no_fuse=False,
										fn_name='gvrc_m_cpy', l_args=l_out_args)
				# fh.write(sm)
			elif emt == e_move_type.emtStoreToStore:
				assert False, 'Not implemented yet'
			else:
				assert False, 'Unkown emt opt'

		else:
			assert False, 'Other move command types not implemented yet.'

class cl_arg_out_data(object):
	def __init__(self, evt, evt_num, core_text):
		self.__evt = evt # none means just pass the core text as a val arg
		self.__evt_num = evt_num
		self.__core_text = core_text
		self.__i_shared = -1

	def get_evt(self):
		return self.__evt

	def get_evt_num(self):
		return self.__evt_num

	def get_core_text(self):
		return self.__core_text

	def set_i_shared(self, i_shared):
		self.__i_shared = i_shared


class cl_line_out_data(object):
	def __init__(self, iline, leading_text, trailing_text, core_text, b_no_fuse, fn_name, l_args):
		self.__iline = iline
		self.__leading_text = leading_text
		self.__trailing_text = trailing_text
		self.__core_text = core_text
		self.__b_no_fuse = b_no_fuse
		self.__fn_name = fn_name
		self.__l_args = l_args # list of cl_arg_out_data

	def write_to_file(self, fh):
		l_core_text_lines = self.__core_text.splitlines()
		if len(l_core_text_lines) < 2:
			fh.write(self.__leading_text+self.__core_text+self.__trailing_text)
		else:
			for one_line in l_core_text_lines:
				fh.write(self.__leading_text + one_line + '\n')

	def get_fn_name(self):
		return self.__fn_name

	def get_no_fuse(self):
		return self.__b_no_fuse

	def get_l_args(self):
		return self.__l_args

	def prefix_core_text(self, prefix):
		self.__core_text = prefix + self.__core_text

	def set_core_text(self, core_text):
		self.__core_text = core_text

	def get_core_text(self):
		return self.__core_text



class cl_out_data(object):
	def __init__(self, fname):
		self.__fname = fname
		self.__l_sm_regs_avail = [True for _ in c_sm_defs_tbl]
		self.__l_rn_regs_avail = [irn < c_max_rn_regs_avail for irn in range(c_max_live_vrs)]
		self.__l_sm_idx_assigned = [-1 for _ in c_sm_defs_tbl]
		self.__l_rn_idx_assigned = [-1 for _ in range(c_max_live_vrs)]
		self.__l_line_data = [] # list of cl_line_out_data

	def get_fname(self):
		return self.__fname

	def set_sm_reg_used(self, ism):
		self.__l_sm_regs_avail[ism] = False

	def is_sm_avail(self, ism):
		return self.__l_sm_regs_avail[ism]

	def add_sm_idx(self, imrk, ism):
		self.__l_sm_idx_assigned[imrk] = ism

	def get_sm_idx(self, imrk):
		return self.__l_sm_idx_assigned[imrk]

	def set_rn_reg_used(self, irn):
		self.__l_rn_regs_avail[irn] = False

	def is_rn_avail(self, irn):
		return self.__l_rn_regs_avail[irn]

	def add_rn_idx(self, ireg, irn):
		self.__l_rn_idx_assigned[ireg] = irn

	def get_rn_idx(self, ireg):
		return self.__l_rn_idx_assigned[ireg]

	def add_line(self, iline, leading_text, trailing_text, core_text, b_no_fuse, fn_name, l_args):
		self.__l_line_data.append(cl_line_out_data( iline, leading_text, trailing_text, core_text, b_no_fuse,
													fn_name, l_args))

	def write_file(self):
		fh_out = open(self.__fname, 'wt')
		fh_out.write(g_gvrc_c_hdr)
		for iline, line in enumerate(self.__l_line_data):
			line.write_to_file(fh_out)
		fh_out.close()

	def make_one_fuse(self, l_fusing_lines, d_frag_frag):
		s_fuse_fn = 'static void gen_fused' + '<fuse_num>' + '('
		s_decls_needthis = '{\n' # Need this in apl generation. BUT! set the sm and rn regs directly with vals set for s_gvml_call, core_text: arg.get_core_text()
		s_frag_call_needthis = '\tRUN_IMM_FRAG_ASYNC(gen_frag' + '<fuse_num>' + '(' # Need this in apl generation
		s_gvml_fn = 'void gvrc_fused' + '<fuse_num>' + '('
		s_call = '\tgen_fused' + '<fuse_num>' + '('
		s_fuse_decl = 'void gen_fused' + '<fuse_num>' + '('
		s_frag_needthis = '' # This is the one we're going to end up putting into the function 'as is'
		s_gvml_call = 'gvrc_fused' + '<fuse_num>' + '(' # Need this in apl generation
		s_gvml_decl = 'void gvrc_fused' + '<fuse_num>' + '('
		d_args = dict()
		# l_arg_names = []
		# last_shared_arg = -1
		b_first_frag_arg, b_first_call_arg = True, True
		val_name_num, rn_name_num, sm_name_num = 0, 0, -1
		l_sm_reg_restore = []
		b_line_rollback = False
		for i_fusing_line, line in enumerate(l_fusing_lines):
			s_decls_rollback = s_decls_needthis
			s_frag_rollback = s_frag_needthis
			s_frag_call_rollback = s_frag_call_needthis
			line.prefix_core_text('// ')
			frag_obj = d_frag_frag.get(line.get_fn_name(), None)
			assert frag_obj != None, 'Error. make_one_fuse should only be called for fn in d_frag_frag'
			l_src_frag_lines = frag_obj.get_l_frag_lines()
			l_frag_lines = [s for s in l_src_frag_lines]
			for i_sm_extra, sm_extra in enumerate(frag_obj.get_l_sm_extras()):
				s_frag_call_needthis += '' if (b_first_frag_arg and b_first_call_arg) else ', '
				b_first_frag_arg = False
				while True:
					sm_name_num += 1
					if sm_name_num >= len(c_sm_defs_tbl):
						print('No more available SM REGS for extra of fragfrag. Remaining lines moving to another frag')
						assert i_fusing_line > 0, 'Not enough rn regs avail for a single line'
						b_line_rollback = True
						break
					if c_sm_defs_tbl[sm_name_num][0] and self.is_sm_avail(sm_name_num):
						if c_sm_defs_tbl[sm_name_num][1]:
							l_sm_reg_restore.append(sm_name_num)
						break
				if b_line_rollback:
					break
				s_frag_call_needthis += 'SM_REG s' + str(i_sm_extra) + ' = SM_REG_' + str(sm_name_num)
				for iline in range(len(l_frag_lines)):
					l_frag_lines[iline] = l_frag_lines[iline].replace('<s' + str(i_sm_extra) + '>', 's'+str(i_sm_extra))
				s_decls_needthis += '\tapl_set_sm_reg(SM_REG_' + str(sm_name_num) + ', ' + hex(sm_extra) + ');\n'
			for special_arg in frag_obj.get_l_specials():
				var_decl, var_name, var_val = special_arg
				skey = 's' + var_decl + ':' + var_val
				shared_var_name = d_args.get(skey, '')
				if shared_var_name == '':
					shared_var_name = var_name
					d_args[skey] = var_name
					s_frag_call_needthis += '' if (b_first_frag_arg and b_first_call_arg) else ', '
					# s_frag_call += var_decl + ' ' + var_name + ' = ' + var_val
					b_first_frag_arg = False
					s_frag_call_needthis += var_decl + ' ' + shared_var_name + ' = ' + var_val
				for iline in range(len(l_frag_lines)):
					l_frag_lines[iline] = l_frag_lines[iline].replace('<'+var_name+'>', shared_var_name)
			for iarg, arg in enumerate(line.get_l_args()):
				cstart = 'v' if arg.get_evt() == e_var_type.evtVR else 'm' if arg.get_evt() == e_var_type.evtMRK else 'n'
				if arg.get_evt_num() >= 0:
					skey = cstart + str(arg.get_evt_num())
				else:
					skey = cstart + 'o' + arg.get_core_text()
				shared_var_name = d_args.get(skey, None)
				if shared_var_name == None:
					# last_shared_arg += 1
					b_not_preassigned_req = True
					if c_b_combine_setting:
						if arg.get_evt_num() >= 0:
							if arg.get_evt() == e_var_type.evtVR:
								if self.get_rn_idx(arg.get_evt_num()) != -1: b_not_preassigned_req = False
							elif arg.get_evt() == e_var_type.evtMRK:
								if self.get_sm_idx(arg.get_evt_num()) != -1: b_not_preassigned_req = False
							else:
								assert False, 'Error: arg has evt num but is neither type VR or MRK'
					if b_not_preassigned_req:
						shared_var_name = 'val' + str(val_name_num)
						val_name_num += 1
						# arg.set_i_shared(shared_var_name)
						d_args[skey] = shared_var_name
						opt_comma = '' if b_first_call_arg else ', '
						s_fuse_fn += opt_comma
						s_frag_call_needthis += '' if b_first_frag_arg else ', '
						s_gvml_fn += opt_comma
						s_call += opt_comma
						s_fuse_decl += opt_comma
						s_gvml_call += opt_comma
						s_gvml_decl += opt_comma
						b_first_call_arg, b_first_frag_arg = False, False
						s_decls_needthis += '\t'
						core_text = arg.get_core_text()
						if arg.get_evt() == e_var_type.evtVR:
							if core_text == None:
								core_text = 'GVRC_VR16_0 + avail_info->use_vr[' + str(arg.get_evt_num()) + ']'
							while True:
								if rn_name_num >= c_max_rn_regs_avail:
									print('No more rn regs available for fusing. Cutting to another fuse')
									assert i_fusing_line > 0, 'Not enough rn regs avail for a single line'
									b_line_rollback = True
									break
								if self.is_rn_avail(rn_name_num):
									break
								rn_name_num += 1
							if b_line_rollback:
								break
							s_fuse_fn += 'enum gvrc_vr16 '
							s_decls_needthis += 'apl_set_rn_reg(RN_REG_' + str(rn_name_num)
							s_frag_call_needthis += 'RN_REG ' + shared_var_name + ' = RN_REG_' + str(rn_name_num)
							s_gvml_fn += 'enum gvrc_vr16 '
							s_call += '(enum gvrc_vr16)'
							s_fuse_decl += 'enum gvrc_vr16 '
							s_gvml_decl += 'enum gvrc_vr16 '
							rn_name_num += 1
						else:
							s_fuse_fn += 'u16 '
							if core_text == None:
								core_text = '(enum gvml_mrks_n_flgs)(1 << avail_info->use_mrk[' \
											+ str(arg.get_evt_num()) + '])'
							while True:
								sm_name_num += 1
								if sm_name_num >= len(c_sm_defs_tbl):
									print('No more available SM REGS. Cutting to another fuse.')
									assert i_fusing_line > 0, 'Not enough sm regs avail for a single line'
									b_line_rollback = True
									break
								if c_sm_defs_tbl[sm_name_num][0] and self.is_sm_avail(sm_name_num):
									if c_sm_defs_tbl[sm_name_num][1]:
										l_sm_reg_restore.append(sm_name_num)
									break
							if b_line_rollback:
								break
							# assert sm_name_num <= 15, 'Fuse cannot handle more than 15 SM_REGs'
							s_decls_needthis += 'apl_set_sm_reg(SM_REG_' + str(sm_name_num)
							s_frag_call_needthis += 'SM_REG ' + shared_var_name + ' = SM_REG_' + str(sm_name_num)
							if arg.get_evt() == e_var_type.evtMRK:
								s_gvml_fn += 'enum gvml_mrks_n_flgs '
								s_gvml_decl += 'enum gvml_mrks_n_flgs '
							else:
								s_gvml_fn += 'u16 '
								s_gvml_decl += 'u16 '
							s_call += '(u16)'
							s_fuse_decl += 'u16 '

						s_fuse_fn += 'a' + shared_var_name
						# s_decls_needthis += ', a' + shared_var_name + ');\n'
						s_decls_needthis += ', ' + core_text + ');\n'
						s_gvml_fn += shared_var_name
						s_call += shared_var_name

						s_gvml_decl += shared_var_name
						s_fuse_decl += shared_var_name
						# core_text = arg.get_core_text()
						# if core_text == None:
						# 	if arg.get_evt() == e_var_type.evtVR:
						# 		core_text = 'GVRC_VR16_0 + avail_info->use_vr[' + str(arg.get_evt_num()) + ']'
						# 	else:
						# 		core_text = '(enum gvml_mrks_n_flgs)(1 << avail_info->use_mrk[' + str(arg.get_evt_num()) + '])'
						# s_gvml_call_needthis += core_text
					# end if get_evt() < 0
					else:
						# shared_var_name = cstart + str(val_name_num)
						shared_var_name = cstart + str(arg.get_evt_num())
						d_args[skey] = shared_var_name
						s_frag_call_needthis += '' if b_first_frag_arg else ', '
						b_first_frag_arg = False
						if arg.get_evt() == e_var_type.evtVR:
							s_frag_call_needthis += 'RN_REG ' + shared_var_name + ' = RN_REG_' + str(self.get_rn_idx(arg.get_evt_num()))
						else:
							s_frag_call_needthis += 'SM_REG ' + shared_var_name + ' = SM_REG_' + str(self.get_sm_idx(arg.get_evt_num()))
					# end processing for evt arg (mrk or reg)

				# end if first appearance of shared var name

				for iline in range(len(l_frag_lines)):
					l_frag_lines[iline] = l_frag_lines[iline].replace('<'+str(iarg)+'>', shared_var_name)

			# end loop over args
			if b_line_rollback:
				break
			s_frag_needthis += '\t\t// ' + line.get_fn_name() + '\n'
			for frag_line in l_frag_lines:
				s_frag_needthis += frag_line + '\n'

		l_failed_fusing_lines = []
		if b_line_rollback:
			s_decls_needthis = s_decls_rollback
			s_frag_needthis = s_frag_rollback
			s_frag_call_needthis = s_frag_call_rollback
			l_failed_fusing_lines[:] = l_fusing_lines[i_fusing_line:]
		#end loop over fusing lines
		s_fuse_fn += ') {\n'
		s_frag_call_needthis += ') {\n'
		s_frag_needthis += '\t});\n'
		for irestore in l_sm_reg_restore:
			s_frag_needthis += '\tapl_set_sm_reg(SM_REG_' + str(irestore) + ', ' + c_sm_defs_tbl[irestore][2] + ');\n'
		s_frag_needthis += '}\n'
		s_gvml_fn += ') {\n'
		s_call += ');\n}\n'
		s_gvml_call += ')'
		call_line = copy.deepcopy(l_fusing_lines[0])
		# apl_code = s_fuse_fn + s_decls_needthis + s_frag_call_needthis + s_frag_needthis + s_gvml_fn + s_call
		apl_code = s_decls_needthis + s_frag_call_needthis + s_frag_needthis
		# call_line.set_core_text(s_gvml_call_needthis)
		call_line.set_core_text(apl_code)
		if b_first_call_arg:
			s_fuse_decl += 'void'
			s_gvml_decl += 'void'
		s_fuse_decl += ');\n'
		s_gvml_decl += ');\n'

		return call_line, apl_code, s_fuse_decl, s_gvml_decl, l_failed_fusing_lines

	def create_fused(self, d_frag_frag, l_frag_piece_templates):
		b_in_fuse = False
		l_fusing_lines, l_insert_lines, l_insert_nums = [], [], []
		# fuse_num = 0
		l_apl_code, l_apl_decl, l_gvml_decl = [], [], []
		for iline, line in enumerate(self.__l_line_data):
			if line.get_fn_name() == None and not line.get_no_fuse(): continue
			b_fn_no_fuse = (line.get_fn_name() != None and d_frag_frag.get(line.get_fn_name(), None) == None)
			if b_in_fuse and (line.get_no_fuse() or b_fn_no_fuse):
				# if len(l_fusing_lines) > 1: #otherwise ignore for just one line.
				# comment the original lines of the fuse
				while l_fusing_lines != []:
					insert_line, apl_code, apl_decl, gvml_decl, l_fusing_lines = self.make_one_fuse(l_fusing_lines, d_frag_frag)
					if False and apl_code in l_frag_piece_templates:
						ipiece = l_frag_piece_templates.index(apl_code)
					else:
						ipiece = len(l_frag_piece_templates)
						l_frag_piece_templates.append(apl_code)
						l_apl_code.append(apl_code.replace('<fuse_num>', str(ipiece)))
						l_apl_decl.append(apl_decl.replace('<fuse_num>', str(ipiece)))
						l_gvml_decl.append(gvml_decl.replace('<fuse_num>', str(ipiece)))
					# fuse_num += 1
					insert_line.set_core_text(insert_line.get_core_text().replace('<fuse_num>', str(ipiece)))
					l_insert_lines.append(insert_line)
					l_insert_nums.append(iline)
					# l_fusing_lines = []
				b_in_fuse = False
			if line.get_fn_name() != None:
				fn_frag_obj = d_frag_frag.get(line.get_fn_name(), None)
				if fn_frag_obj == None:
					insert_line = copy.deepcopy(line)
					insert_line.set_core_text('set_seu_regs(avail_info)')
					l_insert_lines.append(insert_line)
					l_insert_nums.append(iline+1)
				else:
					l_fusing_lines.append(line)
					b_in_fuse = True
		for insert_num, insert_line in reversed(list(zip(l_insert_nums, l_insert_lines))):
			self.__l_line_data.insert(insert_num, insert_line)

		return l_apl_code, l_apl_decl, l_gvml_decl

class cl_var_data(object):
	def __init__(self, name, evt):
		self.__name = name
		self.__evt = evt
		self.__first_set = -1
		self.__last_use = -1
		self.__l_used = [] # list of len num_code_lines. default at eutUnused. Otherswise set, read or modified
		self.__l_alive = [] # list same len as l_used, initialized at the same time
		self.__idx_assigned = -1
		self.__l_set_read_pairs = [] # list of pairs, each pair the first set and last read in unrolled series
		self.__b_spilled = False # right now, this flag means that the var has already been used to make space for another, and cannot be used again
		self.__l_read_lines = [] # list of line numbers from the original text where the var is read
		self.__l_set_lines = []
		self.__staged_idx = -1
		self.__stored_idx = -1
		# self.__num_code_lines = -1

	def get_name(self):
		return self.__name

	def get_evt(self):
		return self.__evt

	def get_first_set(self):
		return self.__first_set

	def get_b_spilled(self):
		return self.__b_spilled

	def set_b_spilled(self):
		self.__b_spilled = True

	def set_staged_idx(self, idx):
		self.__staged_idx = idx

	def get_staged_idx(self):
		return self.__staged_idx

	def set_stored_idx(self, idx):
		self.__stored_idx = idx

	def get_stored_idx(self):
		return self.__stored_idx

	# def set_num_code_lines(self, num):
	# 	self.__num_code_lines = num

	def init_used(self, num_code_lines):
		if self.__l_used == []:
			self.__l_used = [e_used_type.eutUnused for _ in range(num_code_lines)]
			self.__l_alive = [False for _ in range(num_code_lines)]

	def add_set_read_pair(self, a_last_set, a_last_read):
		self.__l_set_read_pairs.append((a_last_set, a_last_read))

	def set_alive(self, line_num):
		self.__l_alive[line_num] = True

	def get_alive(self, line_num):
		return self.__l_alive[line_num]

	def get_used_state(self, line_num):
		return self.__l_used[line_num]

	def set_used(self, b_in, b_modify, b_temp, line_num):
		if b_in:
			bfound = False
			for il in range(line_num, len(self.__l_used)):
				if self.__l_used[il] in [e_used_type.eutModify, e_used_type.eutRead]:
					bfound = True
					break
			if not bfound:
				self.__last_use = line_num
			if b_modify:
				self.__l_used[line_num] = e_used_type.eutModify
				self.__l_set_lines.append(line_num)
			else:
				self.__l_used[line_num] = e_used_type.eutRead
			self.__l_read_lines.append(line_num)
		else:
			if b_temp:
				self.__l_used[line_num] = e_used_type.eutTemp
				self.__first_set = len(self.__l_alive)-1 # num total alive lines. Want to allocate temps last
			else:
				self.__l_used[line_num] = e_used_type.eutSet
				bfound = False
				for il in range(line_num):
					if self.__l_used[il] == e_used_type.eutSet:
						bfound = True
						break
				if not bfound:
					self.__first_set = line_num
			self.__l_set_lines.append(line_num)

	def get_next_set(self, line_num):
		for iline, eut in enumerate(self.__l_used[line_num+1:]):
			if eut == e_used_type.eutSet:
				return iline + line_num

	def get_next_read(self, line_num):
		for iline, eut in enumerate(self.__l_used[line_num+1:]):
			if eut in [e_used_type.eutRead, e_used_type.eutModify]:
				return iline + line_num + 1
		return -1

	def get_prev_set(self, line_num):
		for iline, eut in enumerate(reversed(self.__l_used[:line_num])):
			if eut == e_used_type.eutSet:
				return line_num - iline - 1
		return -1

	def get_prev_valid_set(self, line_num, ll_code_flow_lines):
		curr_line_num = line_num
		l_initial_code_flow_objs =  ll_code_flow_lines[line_num]
		while True:
			prev_line_num = self.get_prev_set(curr_line_num)
			if prev_line_num == -1:
				return -1
			l_code_flow_objs = ll_code_flow_lines[prev_line_num]
			b_all_found = True
			for initial_cf_obj in l_code_flow_objs:
				if initial_cf_obj not in l_initial_code_flow_objs:
					b_all_found = False
					break
			if b_all_found:
				return prev_line_num
			curr_line_num = prev_line_num


	def get_l_used(self):
		return self.__l_used

	def get_idx_assigned(self):
		assert False, 'get_idx_assigned deprecated'
		return self.__idx_assigned

	def set_idx_assigned(self, idx):
		self.__idx_assigned = idx


class cl_arg_type(object):
	def __init__(self, b_in, b_modify, b_temp, evt, b_info=False):
		self.__b_in = b_in
		self.__b_modify = b_modify
		self.__b_temp = b_temp
		self.__evt = evt
		self.__b_info = b_info

	def get_b_in(self):
		return self.__b_in

	def get_b_modify(self):
		return self.__b_modify

	def get_b_temp(self):
		return self.__b_temp

	def get_b_info(self):
		return self.__b_info

	def get_evt(self):
		return self.__evt


class cl_func_call(object):
	def __init__(self, line_num, name, l_args, leading_text, trailing_text):
		self.__line_num = line_num
		self.__name = name
		self.__l_args = l_args
		self.__leading_text = leading_text
		self.__trailing_text = trailing_text
		self.__info_obj = None
		self.__info_l_args = []

	def set_info_obj_data(self, info_obj, info_l_args):
		self.__info_obj = info_obj
		self.__info_l_args = info_l_args

	def get_info_obj_data(self):
		return self.__info_obj, self.__info_l_args


	def make_output_line(	self, out_data_obj, info_ptr, d_all_vars, l_s_var_scopes, d_map_var_to_stage, l_map_line_to_used,
							d_map_line_to_move_cmd, d_map_line_to_cmt, d_func_tbl):
		output_comments_and_moves(	out_data_obj, self.get_line_num(), info_ptr, d_map_line_to_move_cmd,
									d_map_line_to_cmt, self.__leading_text, self.__trailing_text)
		l_comments =  d_map_line_to_cmt.get(self.get_line_num(), [])

		if self.__info_obj != None:
			self.__info_obj.make_output_line(	out_data_obj, info_ptr, self.__info_l_args, self.get_line_num(),
												l_s_var_scopes, d_map_var_to_stage, d_all_vars, l_map_line_to_used)
		s = self.__name + '('
		l_out_args = []
		l_arg_defs = d_func_tbl.get(self.__name, None)
		assert l_arg_defs != None, ('Error. Function %s called at line %d with no declaration' \
									% (self.__name, self.get_line_num()))
		for iarg, one_arg in enumerate(self.__l_args):
			one_var = d_all_vars.get(one_arg, None)
			if one_var == None:
				assert iarg < len(l_arg_defs), ('Error. Function %s called at line %d not declared for arg %d' \
									% (self.__name, self.get_line_num(), iarg))
				l_out_args.append(cl_arg_out_data(None if l_arg_defs[iarg] == None else l_arg_defs[iarg].get_evt(),
													-1, one_arg))
				s += one_arg
			else:
				resource_idx = d_map_var_to_stage.get((one_arg, self.__line_num), -1)
				assert resource_idx != -1, ('Error. At time of fn call at line %d, stage resource not allocated' % self.__line_num)
				if one_var.get_evt() == e_var_type.evtMRK:
					s += '(enum gvml_mrks_n_flgs)(1 << ' + info_ptr.get_name() + '->use_mrk[' + str(resource_idx) + '])'
				elif one_var.get_evt() == e_var_type.evtVR:
					s += 'GVRC_VR16_0 + ' + info_ptr.get_name() + '->use_vr[' + str(resource_idx) + ']'
				elif one_var.get_evt() == e_var_type.evtMRK:
					s += 'GVML_VM_0 + ' + info_ptr.get_name() + '->use_vmr[' + str(resource_idx) + ']'
				else:
					print("Error: unknown var type in var table")
				l_out_args.append(cl_arg_out_data(one_var.get_evt(), resource_idx, None))

			if iarg < len(self.__l_args) - 1:
				s += ','
			else:
				s += ')'
		out_data_obj.add_line(	-1, self.__leading_text, self.__trailing_text, s,
								b_no_fuse=False, fn_name=self.__name, l_args=l_out_args)
		# fh.write(s)

	def get_line_num(self):
		return self.__line_num

	def get_l_args(self):
		return self.__l_args

	def get_name(self):
		return self.__name

class cl_info_init(object):
	def __init__(self, line_num, name, leading_text, trailing_text):
		self.__line_num = line_num
		self.__name = name
		self.__leading_text = leading_text
		self.__trailing_text = trailing_text

	def get_line_num(self):
		return self.__line_num

	def make_output_line(self, out_data_obj, num_mrks, num_vrs, num_vmrs):
		s = 'init_avail_info(' + self.__name + ',' + str(num_mrks) + ',' + str(num_vrs) + ',' \
			+ str(num_vmrs) + ')'
		out_data_obj.add_line(	-1, self.__leading_text, self.__trailing_text, s,
								b_no_fuse=True, fn_name=None, l_args=[])
		if not c_b_combine_setting: return
		ism = -1
		for imrk in range(min(c_max_preassigned_mrks, num_mrks)):
			# ism and imrk should be in sync, unless I add code to avoid some isms. Depracated comment!
			# Make sure not only that the sm is is within the table but that we don't even reach the need to restore
			# assert ism < len(c_sm_defs_tbl) and not c_sm_defs_tbl[ism][1], 'Error. Not enough SM REGS even for init_avail'
			while True:
				ism += 1
				assert ism < len(c_sm_defs_tbl) , 'Error. Not enough SM REGS for automatic use of init_avail without restore'
				if (not c_sm_defs_tbl[ism][0]) or c_sm_defs_tbl[ism][1]:
					continue
				s = 'set_mrk_reg(avail_info, SM_REG_' + str(ism) + ', ' + str(imrk) + ')'
				# s = 'apl_set_sm_reg(SM_REG_' + str(ism) + ', (enum gvml_mrks_n_flgs)(1 << avail_info->use_mrk[' + str(imrk) + ']))'
				out_data_obj.add_line(	-1, self.__leading_text, self.__trailing_text, s,
										b_no_fuse=True, fn_name=None, l_args=[])
				out_data_obj.set_sm_reg_used(ism)
				out_data_obj.add_sm_idx(imrk, ism)
				break
		irn = -1
		for ivr in range(min(c_max_preassigned_vrs, num_vrs)):
			# irn and ivr should be in sync, unless I make code like for the sm's.
			irn += 1
			assert ivr < c_max_rn_regs_avail, 'Error. Not enough RN REGS even for init_avail'
			s = 'set_vr_reg(avail_info, RN_REG_' + str(irn) + ', ' + str(ivr) + ')'
			# s = 'apl_set_rn_reg(RN_REG_' + str(irn) + ', GVRC_VR16_0 + avail_info->use_vr[' + str(ivr) +  '])'
			out_data_obj.add_line(-1, self.__leading_text, self.__trailing_text, s,
								  b_no_fuse=True, fn_name=None, l_args=[])
			out_data_obj.set_rn_reg_used(irn)
			out_data_obj.add_rn_idx(ivr, irn)

		s = 'set_seu_regs(avail_info)'
		out_data_obj.add_line(-1, self.__leading_text, self.__trailing_text, s,
							  b_no_fuse=True, fn_name=None, l_args=[])
		# fh.write(s)
		pass

	def get_name(self):
		return self.__name

class cl_prep_new_info(object):
	def __init__(self, line_num, name, l_args, leading_text, trailing_text):
		self.__line_num = line_num
		self.__name = name
		self.__l_args = l_args
		self.__leading_text = leading_text
		self.__trailing_text = trailing_text

	def make_output_line(	self, out_data_obj, info_ptr, info_l_args, fn_line_num, l_s_var_scopes, d_map_var_to_stage,
							d_all_vars, l_map_line_to_used):
		out_data_obj.add_line(	fn_line_num, self.__leading_text, self.__trailing_text,
								'tSVR_AVAIL ' + self.__name + ' = *' + info_ptr.get_name(),
								b_no_fuse=True, fn_name=None, l_args=[])
		# fh.write(self.__leading_text + 'tSVR_AVAIL ' + self.__name + ' = *' + info_ptr.get_name() + self.__trailing_text)
		l_mrks_staged, l_vrs_staged, l_mrks_stored, l_vrs_stored = l_map_line_to_used[fn_line_num]
		for imrk, b_mrk_staged in enumerate(l_mrks_staged):
			if b_mrk_staged:
				out_data_obj.add_line(	fn_line_num, self.__leading_text, self.__trailing_text,
										self.__name + '.mrks[' + self.__name + '.use_mrk[' + str(imrk) + ']] = egvrcUsed',
										b_no_fuse=True, fn_name=None, l_args=[])
				# fh.write(self.__leading_text + self.__name + '.mrks[' + self.__name + '.use_mrk[' + str(imrk) + ']] = egvrcUsed' + self.__trailing_text)
			del imrk, b_mrk_staged
		for ivr, b_vr_staged in enumerate(l_vrs_staged):
			if b_vr_staged:
				out_data_obj.add_line(	fn_line_num, self.__leading_text, self.__trailing_text,
										self.__name + '.vrs[' + self.__name + '.use_vr[' + str(ivr) + ']] = egvrcUsed',
										b_no_fuse=True, fn_name=None, l_args=[])
				# fh.write(self.__leading_text + self.__name + '.vrs[' + self.__name + '.use_vr[' + str(ivr) + ']] = egvrcUsed' + self.__trailing_text)

		""" The old way
		for other_var in l_s_var_scopes[fn_line_num]:
			resource_idx = d_map_var_to_stage.get((other_var.get_name(), fn_line_num), -1)
			if resource_idx == -1: continue
			if other_var.get_evt() == e_var_type.evtMRK:
				fh.write(self.__leading_text + self.__name + '.mrks[' + self.__name + '.use_mrk[' + str(resource_idx) + ']] = egvrcUsed' + self.__trailing_text)
			elif other_var.get_evt() == e_var_type.evtVR:
				fh.write(self.__leading_text + self.__name + '.vrs[' + self.__name + '.use_vr[' + str(resource_idx) + ']] = egvrcUsed' + self.__trailing_text)
			elif other_var.get_evt() == e_var_type.evtVL1:
				fh.write(self.__leading_text + self.__name + '.vmrs[' + self.__name + '.use_vmr[' + str(resource_idx) + ']] = egvrcUsed' + self.__trailing_text)
			else:
				print("Error: unknown var type in var scope")
		"""

		for info_arg in info_l_args:
			info_arg.make_output_line(out_data_obj, fn_line_num, d_all_vars, d_map_var_to_stage)
			# info_ptr,

	def get_line_num(self):
		return self.__line_num

class cl_used_as_arg(object):
	def __init__(self, line_num, info_name, arg_name, leading_text, trailing_text):
		self.__line_num = line_num
		self.__name = arg_name
		self.__info_name = info_name
		self.__leading_text = leading_text
		self.__trailing_text = trailing_text

	def make_output_line(self, out_data_obj, fn_line_num, d_all_vars, d_map_var_to_stage):
		s = self.__info_name + '.'
		one_var = d_all_vars.get(self.__name, None)
		if one_var == None:
			assert False, ("Compiler error at line %d. GVRC_SET_USED_AS_ARG called for unknown arg %s " % (self.__line_num, self.__name))

		use_member = ''
		if one_var.get_evt() == e_var_type.evtMRK:
			s += 'mrks'
			use_member = 'use_mrk'
		elif one_var.get_evt() == e_var_type.evtVR:
			s += 'vrs'
			use_member = 'use_vr'
		elif one_var.get_evt() == e_var_type.evtVL1:
			s += 'vmrs'
			use_member = 'use_vmr'
		else:
			assert False, (	"Compiler error at line %d. GVRC_SET_USED_AS_ARG called for arg %s with unknown type. (?)" % (
							self.__line_num, self.__name))

		resource_idx = d_map_var_to_stage.get((self.__name, fn_line_num), -1)
		assert resource_idx != -1, ("Compiler error at line %d. GVRC_SET_USED_AS_ARG called for arg %s when it is not live" % (self.__line_num, self.__name))
		s += '[' + self.__info_name + '.' + use_member + '[' + str(resource_idx) + ']] = egvrcArg'
		out_data_obj.add_line(fn_line_num, self.__leading_text, self.__trailing_text, s, b_no_fuse=True, fn_name=None, l_args=[])
		# fh.write(s)

	def get_arg_name(self):
		return self.__name

	def get_line_num(self):
		return self.__line_num


class cl_code_flow(object):
	def __init__(self, line_num, bstart, bloop = True):
		self.__bloop = bloop # True means a loop, othewise it's a code branch (if or else)
		self.__line_num = line_num
		self.__bstart = bstart
		self.__end_line_num = -1
		self.__l_state_at_start = []

	def get_bloop(self):
		return self.__bloop

	def get_bstart(self):
		return self.__bstart

	def set_end(self, end_line_num):
		self.__end_line_num = end_line_num

	def get_end_line_num(self):
		return self.__end_line_num

	def get_line_num(self):
		return self.__line_num

	def add_var_at_start(self, var_obj):
		self.__l_state_at_start.append((var_obj, var_obj.get_staged_idx(), var_obj.get_stored_idx()))

	def get_l_state_at_start(self):
		return self.__l_state_at_start


class cl_gvrc(object):
	def __init__(self):
		self.__d_vars = dict()
		self.__d_func_tbl = dict()
		self.__l_func_calls = []
		self.__l_code_flow_objs = []
		self.__d_map_line_num_to_fn = dict()
		self.__main_info_name = ''
		self.__l_prep_new_objs = []
		# self.__l_s_var_scopes = []
		self.__l_use_as_arg_objs = []
		self.__l_code_flow_starts = []
		self.__ll_code_flow_lines = []
		self.__d_map_info_name_to_obj = dict()
		self.__d_map_info_name_to_l_args = dict()

		self.__d_arg_type = dict()
		for vtype, vname in var_opts:
			self.__d_arg_type[vname + 'i'] = cl_arg_type(b_in=True, b_modify=False, b_temp=False, evt=vtype)
			self.__d_arg_type[vname + 'o'] = cl_arg_type(b_in=False, b_modify=False, b_temp=False, evt=vtype)
			self.__d_arg_type[vname + 'm'] = cl_arg_type(b_in=True, b_modify=True, b_temp=False, evt=vtype)
			self.__d_arg_type[vname + 't'] = cl_arg_type(b_in=False, b_modify=False, b_temp=True, evt=vtype)
		# self.__d_arg_type['evtInfo'] = cl_arg_type(b_in=False, b_modify=False, b_temp=False, evt=None, b_info=True)
		self.__d_arg_type['evtNone'] = None

	def write_macro_spread(self, line_num, full_text, fn_name, l_args, fh_macro, lmacro_args,
						   lmacros, dmacros, ltext, ttext):
		ret = False
		if fn_name == 'GVRC_MACRO_CALL':
			ret = True
			assert l_args != [] and l_args[0] != '', ('Macro error. Macro at line %d missing a name'
													  % (line_num))
			imacro = dmacros.get(l_args[0])
			assert imacro != -1, ('Macro error. Unknown macro %s called at line %d'
													  % (l_args[0], line_num))
			assert len(l_args) == len(lmacro_args[imacro]), \
					('Macro error. Macro %s at line %d has %d args instead of %d in definintion'
					 % (l_args[0], line_num, len(l_args) - 1, len(lmacro_args[imacro]) - 1))
			dargs = dict()
			for line_text in lmacros[imacro]:
				for val, arg in zip(l_args, lmacro_args[imacro]):
					line_text = line_text.replace('<'+arg+'>', val)
				fh_macro.write(ltext + line_text + ttext)
		else:
			fh_macro.write(full_text)

		return ret

	def parse_load_macros(	self, line_num, fn_name, l_args, curr_args, lbstarted, line_text, llines, lmacro_args,
							lmacros, dmacros, fh_macro_spread, ltext, ttext):
		if fn_name == 'GVRC_MACRO':
			assert l_args != [] and l_args[0] != '', (	'Macro error. Macro at line %d missing a name'
														% (line_num))
			curr_args[:] = l_args;
			lbstarted[0] = True
			return True
		elif fn_name == 'GVRC_MACRO_END':
			assert lbstarted[0], ('Macro error. Macro %s on line %d ends without a matching start'
												% (l_args[0], line_num))
			assert l_args[0] == curr_args[0], ('Macro error. Macro %s on line %d must end with the same name as start'
												% (curr_args[0], line_num))
			lbstarted[0] = False
			dmacros[curr_args[0]] = len(lmacros)
			lmacro_args.append(list(curr_args))
			lmacros.append(list(llines))
			llines[:] = []
			curr_args[:] = []

		elif lbstarted[0]:
			llines.append(line_text.strip())
		else:
			fh_macro_spread.write(line_text)

		return False

	def parse_fn(self, gvr_path, line_num, fn_name, l_args, leading_text, trailing_text):
		if fn_name == 'GVRC_VAR':
			try:
				lievt = [item[1] for item in var_opts]
				evt = var_opts[lievt.index(l_args[0])][0] # var_opts[l_args[0]][0]
			except:
				assert False, ('Error! Line %d has unrecognized var type %s'% (line_num, l_args[0]))
			var_name = l_args[1]
			var_data = self.__d_vars.get(var_name, None)
			assert var_data == None, ('Error line %d GVRC_VAR called twice for the same var %s'% (line_num, var_name))
			self.__d_vars[var_name] = cl_var_data(var_name, evt)
		elif fn_name == 'GVRC_DECL':
			func_name = l_args[0]
			l_func_args = []
			for one_arg in l_args[1:]:
				arg_obj = self.__d_arg_type.get(one_arg, None)
				# assert arg_obj != None, ('Error! Line %d has has unrecognized var type %s'% (line_num, arg_obj))
				l_func_args.append((arg_obj))
			self.__d_func_tbl[func_name] = l_func_args
		elif fn_name == 'GVRC_FN':
			func_name = l_args[0]
			self.__l_func_calls.append(cl_func_call(line_num, func_name, l_args[1:], leading_text, trailing_text))
			self.__d_map_line_num_to_fn[line_num] = self.__l_func_calls[-1]
		elif fn_name == 'GVRC_LOOP_START':
			self.__l_code_flow_objs.append(cl_code_flow(line_num, bstart=True, bloop=True))
		elif fn_name == 'GVRC_LOOP_END':
			self.__l_code_flow_objs.append(cl_code_flow(line_num, bstart=False, bloop=True))
		elif fn_name == 'GVRC_COND_START':
			self.__l_code_flow_objs.append(cl_code_flow(line_num, bstart=True, bloop=False))
		elif fn_name == 'GVRC_COND_END':
			self.__l_code_flow_objs.append(cl_code_flow(line_num, bstart=False, bloop=False))
		elif fn_name == 'GVRC_INIT':
			self.__main_info_name = cl_info_init(line_num, l_args[0], leading_text, trailing_text)
			self.__d_map_line_num_to_fn[line_num] = self.__main_info_name
		elif fn_name == 'GVRC_PREP_NEW_INFO':
			self.__l_prep_new_objs.append(cl_prep_new_info(line_num, l_args[0], l_args, leading_text, trailing_text))
			# self.__d_map_line_num_to_fn[line_num] = self.__l_prep_new_objs[-1]
			assert self.__d_map_info_name_to_obj.get(l_args[0], None) == None, ('Error at line %d. The same info name may not be used twice.' % line_num)
			self.__d_map_info_name_to_obj[l_args[0]] = self.__l_prep_new_objs[-1]
		elif fn_name == 'GVRC_SET_USED_AS_ARG':
			assert len(l_args) == 2, ('GVRC_SET_USED_AS_ARG needs exactly two args, <info-obj-name> and <arg-name>')
			info_name = l_args[0]
			arg_name = l_args[1]
			self.__l_use_as_arg_objs.append(cl_used_as_arg(line_num, info_name, arg_name, leading_text, trailing_text))
			# self.__d_map_line_num_to_fn[line_num] = self.__l_use_as_arg_objs[-1]
			l_info_args = self.__d_map_info_name_to_l_args.get(info_name, [])
			l_info_args.append(self.__l_use_as_arg_objs[-1])
			self.__d_map_info_name_to_l_args[info_name] = l_info_args
		elif fn_name == 'GVRC_INCLUDE':
			include_fname = l_args[0]
			# path_in, _ = os.path.split(g_fnt_in)
			full_include_fname = include_fname + '.gvri'
			l_included_text = []
			self.read_from_file(gvr_path, gvr_path, full_include_fname, l_included_text)

		else:
			assert False, ('Error. Unknown fn name %s at line %d'% (fn_name, line_num))

	def allocate_staged_and_stored(	self, one_var, iline, l_staged, l_stored, l_s_var_scopes, num_code_lines,
									d_map_line_to_move_cmd, d_map_var_to_stage, d_map_line_to_cmt):
		num_needing_alloc = 0
		if one_var.get_used_state(iline) == e_used_type.eutUnused: return 0
		istage = one_var.get_staged_idx()
		if istage == -1:
			# First try and find a free staged mrk
			istage = next((imrk for (imrk, bused) in enumerate(l_staged) if not bused), -1)
			if num_needing_alloc < istage +1:
				num_needing_alloc = istage + 1
			if istage == -1:
				# if they are all taken, we have to spill some staged mark
				l_vars_avail_for_spill = []
				for other_var in l_s_var_scopes[iline]:
					if other_var is self: continue
					if other_var.get_evt() != one_var.get_evt(): continue
					if other_var.get_staged_idx() == -1: continue  # shouldn't happen
					# we need the spilled mark to be not active on this specific line of code
					if other_var.get_used_state(iline) == e_used_type.eutUnused:
						l_vars_avail_for_spill.append(other_var)
				# now we find the var that's not needed for the longest time
				max_dist_till_needed = -1
				best_spill_var = None
				assert l_vars_avail_for_spill != [], ('No var avail to spill on line %d' % iline)
				for spill_var in l_vars_avail_for_spill:
					for iline2 in range(iline, num_code_lines):
						if spill_var.get_used_state(iline2) != e_used_type.eutUnused:
							if (iline2 - iline) > max_dist_till_needed:
								max_dist_till_needed = iline2 - iline
								best_spill_var = spill_var
								break
				if best_spill_var == None: # Can happen if all avail vars have no later read but only earlier within loop
					# could be calulated instead of arbitrary but must go through all loops of the code line from inner to outer
					best_spill_var = l_vars_avail_for_spill[0]
				# assert best_spill_var != None, ('No place to spill var on line %d' % iline)
				# The mrk to be spilled, might already be stored. We only need to store if it is not
				istage = best_spill_var.get_staged_idx()
				if best_spill_var.get_stored_idx() == -1:
					istore = next((imrk for (imrk, bused) in enumerate(l_stored) if not bused), -1)
					assert istore != -1, ('No place to store var to on line %d' % iline)
					l_comments = d_map_line_to_cmt.get(iline, [])
					l_comments.append('// line %d. spill %s from %d to %d' % (iline, best_spill_var.get_name(), istage, istore))
					d_map_line_to_cmt[iline] = l_comments
					l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
					l_move_cmds.append((iline, best_spill_var, istage, istore, e_move_type.emtStageToStore))
					d_map_line_to_move_cmd[iline] = l_move_cmds
					best_spill_var.set_stored_idx(istore)
					l_stored[istore] = True
				best_spill_var.set_staged_idx(-1)
			# This is reached whether the var was already staged or not
			# Make sure anyway that the var and the l_staged list knows we're here.
			one_var.set_staged_idx(istage)
			l_comments = d_map_line_to_cmt.get(iline, [])
			l_comments.append(	'// line %d. stage alloc %s to %d' %
								(iline, one_var.get_name(), istage))
			d_map_line_to_cmt[iline] = l_comments
			# If this is a read and the var is live, we expect that it is stored, we need to copy it to stage
			if one_var.get_used_state(iline) in [e_used_type.eutRead, e_used_type.eutModify]:
				assert one_var.get_stored_idx() != -1, \
					('Error. Live var on line %d needed for read yet not staged' % iline)
				l_comments = d_map_line_to_cmt.get(iline, [])
				l_comments.append(	'// line %d. restore %s from %d to %d' %
									(iline, one_var.get_name(),  one_var.get_stored_idx(), istage))
				d_map_line_to_cmt[iline] = l_comments
				l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
				l_move_cmds.append((iline, one_var, one_var.get_stored_idx(), istage, e_move_type.emtStoreToStage))
				d_map_line_to_move_cmd[iline] = l_move_cmds
			l_staged[istage] = True
		# Tell the output where the var is staged
		d_map_var_to_stage[(one_var.get_name(), iline)] = istage
		# If we are setting or modifying the var and it has a stored location, invalidate that, because the data has changed
		if one_var.get_stored_idx() != -1 and one_var.get_used_state(iline) in [e_used_type.eutSet, e_used_type.eutModify]:
			l_stored[one_var.get_stored_idx()] = False
			one_var.set_stored_idx(-1)
		return num_needing_alloc

	# Horrifically complex function is actually that way because of all the possibilities involved in
	# restoring both the staged and stored to exactly the way the were at the start of a loop/cond
	# Note. This function is unlikely to be fully tested. There are many possibilities that are both
	# rare and are very difficult to force to happen. Therefore code paths may never have been tested.
	def restore_loop_state_at_start(self, one_var, stage_state_idx, store_state_idx, iline, l_staged, l_stored,
									l_s_var_scopes, d_map_line_to_move_cmd, d_map_line_to_cmt):
		if stage_state_idx != -1 and one_var.get_staged_idx() != stage_state_idx:
			# need to move the var to the state_idx
			# if it's currently in use, free it
			if l_staged[stage_state_idx]:
				# find who'se got it
				grabbing_var = None
				for other_var in l_s_var_scopes[iline]:
					if other_var.get_evt() != one_var.get_evt() \
							or other_var.get_staged_idx() != stage_state_idx: continue
					grabbing_var = other_var
					print('run catch at line %d restoring %s grabbed by %s' % (iline, one_var.get_name(), other_var.get_name()))
					# assert False, 'Code here never run. Check results and remove assertion'
					break
				assert grabbing_var != None, 'Makes no sense that stage var is grabbed but nobody has it'
				if grabbing_var.get_stored_idx() == -1:
					istore = next((imrk for (imrk, bused) in enumerate(l_stored) if not bused), -1)
					assert istore != -1, ('No place to store var to on line %d' % iline)
					l_comments = d_map_line_to_cmt.get(iline, [])
					l_comments.append('// recreate loop start at line %d. spill %s from %d to %d' %
									  (iline, grabbing_var.get_name(), stage_state_idx, istore))
					d_map_line_to_cmt[iline] = l_comments
					l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
					l_move_cmds.append((iline, grabbing_var, stage_state_idx, istore, e_move_type.emtStageToStore))
					d_map_line_to_move_cmd[iline] = l_move_cmds
					grabbing_var.set_stored_idx(istore)
					grabbing_var.set_staged_idx(-1) # Not needed for every spill to store but important here because we are the grabber
					l_stored[istore] = True
					l_staged[stage_state_idx] = False
					# assert False, 'Code here never run. Check results and remove assertion'
			if one_var.get_stored_idx() != -1:
				l_comments = d_map_line_to_cmt.get(iline, [])
				l_comments.append('// recreate loop start at line %d. restore %s from %d to %d' %
								  (iline, one_var.get_name(), one_var.get_stored_idx(), stage_state_idx))
				d_map_line_to_cmt[iline] = l_comments
				l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
				l_move_cmds.append((iline, one_var, one_var.get_stored_idx(), stage_state_idx,
									e_move_type.emtStoreToStage))
				d_map_line_to_move_cmd[iline] = l_move_cmds
				# if there was a different place it was staged at, even though we used the store, we must clean out the old stage
				if one_var.get_staged_idx() != -1:
					l_staged[one_var.get_staged_idx()] = False
				# important. If during the loop, var was stored but was not stored before, we have to clean this out to restore ante bellum
				if store_state_idx == -1:
					l_stored[one_var.get_stored_idx()] = False
					one_var.set_stored_idx(-1)
			elif one_var.get_staged_idx() != -1:
				l_comments = d_map_line_to_cmt.get(iline, [])
				l_comments.append('// recreate loop start at line %d. recreate staged %s from %d to %d' %
								  (iline, one_var.get_name(), one_var.get_staged_idx(), stage_state_idx))
				d_map_line_to_cmt[iline] = l_comments
				l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
				l_move_cmds.append((iline, one_var, one_var.get_staged_idx(), stage_state_idx,
									e_move_type.emtStageToStage))
				d_map_line_to_move_cmd[iline] = l_move_cmds
				l_staged[one_var.get_staged_idx()] = False
			else:
				assert False, 'Error. Live var is neither staged nor stored at loop restore'
			l_staged[stage_state_idx] = True
			one_var.set_staged_idx(stage_state_idx)
		if store_state_idx != -1 and one_var.get_stored_idx() != store_state_idx:
			# need to move the var to the state_idx
			# if it's currently in use, free it
			if l_stored[store_state_idx]:
				# find who'se got it
				grabbing_var = None
				for other_var in l_s_var_scopes[iline]:
					if other_var.get_evt() != one_var.get_evt() \
							or other_var.get_stored_idx() != store_state_idx: continue
					grabbing_var = other_var
					assert False, 'Code here never run. Check results and remove assertion'
					break
				assert grabbing_var != None, 'Makes no sense that store var is grabbed but nobody has it'
				istore = next((imrk for (imrk, bused) in enumerate(l_stored) if not bused), -1)
				assert istore != -1, ('No place to store var to on line %d' % iline)
				l_comments = d_map_line_to_cmt.get(iline, [])
				l_comments.append('// recreate loop start at line %d. move away stored %s from %d to %d' %
								  (iline, grabbing_var.get_name(), store_state_idx, istore))
				d_map_line_to_cmt[iline] = l_comments
				l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
				l_move_cmds.append((iline, grabbing_var, store_state_idx, istore, e_move_type.emtStoreToStore))
				d_map_line_to_move_cmd[iline] = l_move_cmds
				grabbing_var.set_stored_idx(istore)
				l_stored[istore] = True
				l_stored[store_state_idx] = False
				assert False, 'Code here never run. Check results and remove assertion'
			if one_var.get_stored_idx() != -1:
				l_comments = d_map_line_to_cmt.get(iline, [])
				l_comments.append('// recreate loop start at line %d. recreate store %s from %d to %d' %
								  (iline, one_var.get_name(), one_var.get_stored_idx(), store_state_idx))
				d_map_line_to_cmt[iline] = l_comments
				l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
				l_move_cmds.append((iline, one_var, one_var.get_stored_idx(), store_state_idx,
									e_move_type.emtStoreToStore))
				d_map_line_to_move_cmd[iline] = l_move_cmds
				l_stored[one_var.get_stored_idx()] = False
				assert False, 'Code here never run. Check results and remove assertion'
			elif one_var.get_staged_idx() != -1:
				l_comments = d_map_line_to_cmt.get(iline, [])
				l_comments.append('// recreate loop start at line %d. spill %s from %d to %d' %
								  (iline, one_var.get_name(), one_var.get_staged_idx(), store_state_idx))
				d_map_line_to_cmt[iline] = l_comments
				l_move_cmds = d_map_line_to_move_cmd.get(iline, [])
				l_move_cmds.append((iline, one_var, one_var.get_staged_idx(), store_state_idx,
									e_move_type.emtStageToStore))
				d_map_line_to_move_cmd[iline] = l_move_cmds
			else:
				assert False, 'Error. Live var is neither staged no stored at loop restore'
			l_stored[store_state_idx] = True
			one_var.set_stored_idx(store_state_idx)


	def process(self, l_orig_text, out_data_obj):
		num_lines = len(l_orig_text)
		# This section matches loop starts with its ends
		code_flow_obj_stack = []
		d_map_line_to_code_flow_obj = dict()
		for code_flow_obj in self.__l_code_flow_objs:
			if code_flow_obj.get_bstart():
				code_flow_obj_stack.append(code_flow_obj)
				self.__l_code_flow_starts.append(code_flow_obj)
				d_map_line_to_code_flow_obj[code_flow_obj.get_line_num()] = code_flow_obj
			else:
				assert code_flow_obj_stack != [], ('Error at line %d. Unmatched loop ending' % code_flow_obj.get_line_num())
				start_obj = code_flow_obj_stack.pop()
				start_obj.set_end(code_flow_obj.get_line_num())
				# make sure that a cond_end is not matching a loop_end
				assert start_obj.get_bloop() == code_flow_obj.get_bloop(), ('Error at line %d matching loop with code' % code_flow_obj.get_line_num())
				d_map_line_to_code_flow_obj[code_flow_obj.get_line_num()] = start_obj
			del code_flow_obj
		assert code_flow_obj_stack == [], ('Error at line %d. Unmatched loop start' % code_flow_obj_stack[-1].get_line_num())

		# this section removes the end objects now that there is a code flow object with start and end data members
		# self.__ll_code_flow_lines is a list of a list of all code flows for all lines.
		# So if a line is part of a deeply nested set of loops and conds, all of them need to be records
		l_curr_code_flow_objs = []
		for iline in range(num_lines):
			code_flow_obj = d_map_line_to_code_flow_obj.get(iline, None)
			if code_flow_obj != None:
				if code_flow_obj.get_line_num() == iline:
					l_curr_code_flow_objs.append(code_flow_obj)
				elif code_flow_obj.get_end_line_num() == iline:
					l_curr_code_flow_objs.remove(code_flow_obj)
			self.__ll_code_flow_lines.append(list(l_curr_code_flow_objs))

		# This section goes through the func calls, verifying arguments and setting the use locations for
		# those arguments
		for func_call in self.__l_func_calls:
			func_name = func_call.get_name()
			l_args = self.__d_func_tbl.get(func_name, None)
			assert l_args != None, ('Error at line %d. function %s not declared.' % (func_call.get_line_num(), func_name))
			l_passed = func_call.get_l_args()
			assert len(l_args) == len(l_passed), ('Error at line %d. %d args passed instead of %d.'
												  % (func_call.get_line_num(), len(l_passed), len(l_args)))
			d_map_line_to_info_obj = dict()
			for arg_name, arg_type in zip(l_passed, l_args):
				if arg_type != None and arg_type.get_b_info():
					info_obj = self.__d_map_info_name_to_obj.get(arg_name, None)
					assert info_obj != None, ('Compiler error at line %d. %s has not been declared as an info obj.' % (func_call.get_line_num(), arg_name))
					func_call.set_info_obj_data(info_obj, self.__d_map_info_name_to_l_args.get(arg_name, []))
					continue
				arg = self.__d_vars.get(arg_name, None)
				if arg == None:
					continue
				assert arg.get_evt() == arg_type.get_evt(), \
						('Compiler error at line %d. arg %s is not declared as the correct type for this function'
								% (func_call.get_line_num(), arg_name))
				b_in = arg_type.get_b_in()
				b_modify = arg_type.get_b_modify()
				b_temp = arg_type.get_b_temp()
				arg.init_used(num_lines)
				arg.set_used(b_in, b_modify, b_temp, func_call.get_line_num())

		for prep_call in self.__l_prep_new_objs:
			arg = self.__d_vars.get(arg_name, None)
			# if arg == None:

		# This section calculates the keep_alive status of each variable
		# First sort the vars by first set. This allows us to process them in the order they appear in the code
		# Otherwise somebody early may grab a staged location before it needs it
		l_s_var_scopes = [set() for _ in range(num_lines)]
		num_mrks, num_vrs, num_vmrs = 0, 0, 0
		l_vars = sorted([one_var for one_var in self.__d_vars.values()], key=lambda x: x.get_first_set())
		for one_var in l_vars:
			l_used = one_var.get_l_used()
			balive = False
			keep_alive_till = -1
			for iline, eut in enumerate(l_used):
				if eut == e_used_type.eutUnused: # we're only interested in *events* for the var
					# However, don't continue, because there might be some more stuff needed later on
					pass
				elif eut in [e_used_type.eutSet, e_used_type.eutTemp]:
					# if eut == e_used_type.eutTemp:
					# 	print('Here is a temp')
					# If you're setting, you want to know where there is a read that depends on this set
					# There might be a conditional area that has a read preceeded by its own set, this has to
					# be discounted
					curr_line_num = iline
					while True:
						next_line_num = one_var.get_next_read(curr_line_num)
						if next_line_num == -1:
							break
						# ie somewhere in the code later than the set, there a read
						set_of_next_read = one_var.get_prev_valid_set(next_line_num, self.__ll_code_flow_lines)
						assert set_of_next_read != -1, \
								('Error! The read or modify on line %d is called for var %s whose only previous set is conditional and therefore may not have occurred.'
								% (next_line_num, one_var.get_name()))
						if set_of_next_read <= iline:
							balive = True # otherwise, the next read has a better set to rely on
							if next_line_num > keep_alive_till:
								keep_alive_till = next_line_num
							break
						curr_line_num = next_line_num


					if keep_alive_till < iline and not balive:
						# in this case, nobody needs the set so we set the keep_alive for this line only
						if eut == e_used_type.eutSet: # and not a temp
							print(('Warning. var %s at line %d seems to be set without a read. Hope theres a good reason for this' % (one_var.get_name(), iline)))
						keep_alive_till = iline
						balive = True
					# if not balive and keep_alive_till <= iline:
					# 	keep_alive_till = iline + 1
					# 	balive = True
				elif eut in [e_used_type.eutRead, e_used_type.eutModify]:
					# If it's a read, look ahead to see whether there are more reads that depended on this read's set
					# Only one read further is needed, leaving that next read to do the same check for further reads
					prev_set_line_num = one_var.get_prev_valid_set(iline, self.__ll_code_flow_lines)
					assert prev_set_line_num != -1, ('Error! read or modify of var %s on line %d called without unconditional set'
													 % (one_var.get_name(), iline))
					next_line_num = one_var.get_next_read(iline)
					if next_line_num != -1:
						set_of_next_read = one_var.get_prev_valid_set(next_line_num, self.__ll_code_flow_lines)
						if set_of_next_read <= iline:
							balive = True  # otherwise, the next read has a better set to rely on
							if next_line_num > keep_alive_till:
								keep_alive_till = next_line_num
					# if not balive and keep_alive_till <= iline:
					# 	keep_alive_till = iline + 1
					# 	balive = True
				else:
					assert False, 'There are no more eut_type s'

				if eut != e_used_type.eutUnused:
					# For loop objects, if I enter the loop with a live var, I must end it with a live var
					# This means that if some event (set or read) made it end the keep alive early, then extend it to the
					# end of the loop.
					# This is not true for COND. If there is a read beyond the cond, it will be kept alive anyway beyond the end
					# and if there is no read beyond the end of the cond, then there is no need to keep alive till the
					# end of the cond
					l_code_flow_objs = self.__ll_code_flow_lines[iline]
					for cf_obj in l_code_flow_objs:
						if not cf_obj.get_bloop(): continue
						if cf_obj.get_end_line_num() > keep_alive_till:
							initial_line_num = cf_obj.get_line_num()
							if one_var.get_alive(initial_line_num):
								keep_alive_till = cf_obj.get_end_line_num()
								balive = True
						del cf_obj

				# if alive, set the internal var data to say this and add the var to the scopes list for that line
				if balive:
					one_var.set_alive(iline)
					l_s_var_scopes[iline].add(one_var)

				# If this is the last line we needed to be alive, remove the balive
				if balive and keep_alive_till == iline:
					keep_alive_till = -1
					balive = False
				del iline, eut
			#end of loop over lines
			del one_var
		# end of loop over vars

		l_mrks_staged = [False for _ in range(c_max_live_marks)]
		l_vrs_staged = [False for _ in range(c_max_live_vrs)]
		l_mrks_stored = [False for _ in range(c_max_stored_marks)]
		l_vrs_stored = [False for _ in range(c_max_stored_vrs)]
		d_map_var_to_stage = dict()
		d_map_line_to_move_cmd = dict()
		d_map_line_to_cmt = dict()
		l_map_line_to_used = []
		for iline in range(num_lines):
			# We need to do a number of things for each line:
			# 1. We need to free up any vars from staging and storage if the var moves from live to not
			if iline > 0:
				for one_var in l_s_var_scopes[iline - 1]:
					if one_var in l_s_var_scopes[iline]: continue
					if one_var.get_staged_idx() != -1:
						if one_var.get_evt() == e_var_type.evtMRK:
							l_mrks_staged[one_var.get_staged_idx()] = False
						elif one_var.get_evt() == e_var_type.evtVR:
							l_vrs_staged[one_var.get_staged_idx()] = False
						else:
							assert False, 'Error. only mrks and vrs implements as yet'
						one_var.set_staged_idx(-1)
					if one_var.get_stored_idx() != -1:
						if one_var.get_evt() == e_var_type.evtMRK:
							l_mrks_stored[one_var.get_stored_idx()] = False
						elif one_var.get_evt() == e_var_type.evtVR:
							l_vrs_stored[one_var.get_stored_idx()] = False
						else:
							assert False, 'Error. only mrks and vrs implements as yet'
						one_var.set_stored_idx(-1)
					del one_var
			# 2. If we are at a loop/cond start we need to record the positions of all staged and stored vars
			cf_obj = d_map_line_to_code_flow_obj.get(iline, None)
			if cf_obj != None: # NO SECOND CLAUSE. see note below and cf_obj.get_bloop():
				if cf_obj.get_line_num() == iline:
					for one_var in l_s_var_scopes[iline]:
						cf_obj.add_var_at_start(one_var)
						del one_var
				elif cf_obj.get_end_line_num() == iline:
					# 3. If we are at a loop or cond end, we need to restore the staged and stored to the positions at the loop start
					# For a loop, the reason is because, we might be going around again
					# However, for a cond (and a loop too), there is the danger that the loop is never entered
					# Code beyond the cond/loop must behave exactly the same whether the processing entered the cond or not
					# So in both cases, we must restore
					# BUT BIG DIFFERENCE. For cond, only vars live at the end must be restored. For loop. All of them
					l_state_at_start = cf_obj.get_l_state_at_start()
					for one_state in l_state_at_start:
						one_var, stage_state_idx, store_state_idx = one_state
						if not cf_obj.get_bloop() and one_var not in l_s_var_scopes[iline]:
							continue
						if one_var.get_evt() == e_var_type.evtMRK:
							self.restore_loop_state_at_start(	one_var, stage_state_idx, store_state_idx, iline,
																l_mrks_staged, l_mrks_stored, l_s_var_scopes,
																d_map_line_to_move_cmd, d_map_line_to_cmt)
						elif one_var.get_evt() == e_var_type.evtVR:
							self.restore_loop_state_at_start(	one_var, stage_state_idx, store_state_idx, iline,
																l_vrs_staged, l_vrs_stored, l_s_var_scopes,
																d_map_line_to_move_cmd, d_map_line_to_cmt)

						del one_state, one_var, stage_state_idx, store_state_idx
				else:
					assert False, 'A code flow obj should match either start or end'
			del cf_obj
			# 4. We need to make sure all the active mrks and vrs for this line are staged
			for one_var in l_s_var_scopes[iline]:
				if one_var.get_evt() == e_var_type.evtMRK:
					top_mrks = self.allocate_staged_and_stored(	one_var, iline, l_mrks_staged, l_mrks_stored,
																l_s_var_scopes, num_lines, d_map_line_to_move_cmd,
																d_map_var_to_stage, d_map_line_to_cmt)
					if top_mrks > num_mrks: num_mrks = top_mrks
				elif one_var.get_evt() == e_var_type.evtVR:
					top_vrs = self.allocate_staged_and_stored(	one_var, iline, l_vrs_staged, l_vrs_stored,
																l_s_var_scopes, num_lines, d_map_line_to_move_cmd,
																d_map_var_to_stage, d_map_line_to_cmt)
					if top_vrs > num_vrs: num_vrs = top_vrs
				del one_var
			l_map_line_to_used.append((list(l_mrks_staged), list(l_vrs_staged), list(l_mrks_stored), list(l_vrs_stored)))
			del iline

		if d_map_line_to_move_cmd != dict():
			print('Warning. Some spill commands required.')

		# fn_out = expanduser(g_fnt_in+'.c')
		# fh_out = open(fn_out, 'wt')
		for iline, orig_line in enumerate(l_orig_text):
			fn_obj = self.__d_map_line_num_to_fn.get(iline+1, None)
			if fn_obj == None:
				# fh_out.write(orig_line)
				b_empty = (orig_line == '' or orig_line.isspace())
				out_data_obj.add_line(iline, '', '', orig_line, b_no_fuse=not b_empty, fn_name=None, l_args=[])

				output_comments_and_moves(	out_data_obj, iline+1, self.__main_info_name, d_map_line_to_move_cmd,
											d_map_line_to_cmt, '\t\t', ';\n')
			else:
				if type(fn_obj) == cl_info_init:
					fn_obj.make_output_line(out_data_obj, num_mrks, num_vrs, num_vmrs)
				elif type(fn_obj) == cl_func_call:
					fn_obj.make_output_line(out_data_obj, self.__main_info_name, self.__d_vars, l_s_var_scopes,
											d_map_var_to_stage, l_map_line_to_used,
											d_map_line_to_move_cmd, d_map_line_to_cmt,
											self.__d_func_tbl)
				elif type(fn_obj) == cl_prep_new_info:
					fn_obj.make_output_line(out_data_obj, self.__main_info_name, l_s_var_scopes, d_map_var_to_stage)
				elif type(fn_obj) == cl_used_as_arg:
					fn_obj.make_output_line(out_data_obj, self.__d_vars, d_map_var_to_stage)
				else:
					print('Error! Unknown obj type in line table')
		# fh_out.close()


		# fh_out.write(l)

		pass


	def read_from_file(self, gvr_path, fin_path, fname_in, l_orig_text, b_load_macros = False,
					   lmacro_args = [], lmacros = [], dmacros = None, fh_macro_spread = None, lb_keep_spreading = [False]):
		fname_full_in = os.path.join(fin_path, fname_in)
		curr_args = []; lbstarted = [False]; llines = []
		b_has_macro = False
		with open(fname_full_in, 'rb') as fh_in:
			for iline, l in enumerate(iter(fh_in.readline, b'')):
				# l = fh.readline()
				s = l.decode('utf-8')
				comment_idx = s.find('//')
				if comment_idx != -1:
					s = s.split('//')[0] + '\n'
				pat_idx = s.find(GVRC_PAT)
				b_apply_parsing = True
				if pat_idx == -1:
					l_orig_text.append(s)
					b_apply_parsing = False
				else:
					l_orig_text.append('')
				l_args = []
				if b_apply_parsing:
					m = re.match(r"(?P<lw>\s*)(?P<gfn>GVRC_\w*)(?P<mw>\s*)\((?P<contents>.*)\)(?P<tw>[\s;]*)", s)
					m_args = re.findall(r"([^,]*,*)", m.group('contents'))
					for oarg in m_args:
						if oarg == '':
							continue
						m_oarg = re.match(r"\s*(?P<oarg>\w*),?", oarg)
						m_proc = re.match(r".*(?P<proc>[\+\-\/\*\[\]\(\)]).*", oarg)
						if m_oarg == None or m_oarg.group('oarg') == '' or (m_proc != None and m_proc.group('proc') != ''):
							r = re.match(r"\s*(?P<pre>[^,]*)(?P<c>,?)", oarg)
							if r == None or r.group('pre') == '':
								l_args.append(oarg)
							else:
								l_args.append(r.group('pre'))
						else:
							l_args.append(m_oarg.group('oarg'))
					fn_name = m.group('gfn')
					ltext = m.group('lw')
					ttext =  m.group('tw')
				else:
					fn_name = ''; ltext = ''; ttext = ''

				if b_load_macros:
					b_macro_started = self.parse_load_macros(	iline+1, fn_name, l_args, curr_args, lbstarted, l,
																llines, lmacro_args, lmacros, dmacros, fh_macro_spread,
																ltext, ttext)
					if (b_macro_started):
						b_has_macro = True
				elif lb_keep_spreading[0]:
					b_macro_called_here = self.write_macro_spread(	iline+1, l,  fn_name, l_args, fh_macro_spread,
																	lmacro_args, lmacros, dmacros, ltext, ttext)
					if b_macro_called_here:
						b_has_macro = True
				elif b_apply_parsing:
					self.parse_fn(gvr_path, iline + 1, fn_name, l_args, ltext, ttext)
				pass
			# end loop over lines
		# end with open read
		if lb_keep_spreading[0] and not b_has_macro:
			lb_keep_spreading[0] = False

class cl_frag_data(object):
	def __init__(self, fn_name, l_specials, l_sm_extras, l_frag_lines):
		self.__fn_name = fn_name
		self.__l_specials = l_specials
		self.__l_sm_extras = l_sm_extras
		self.__l_frag_lines = l_frag_lines

	def get_l_specials(self):
		return self.__l_specials

	def get_l_sm_extras(self):
		return self.__l_sm_extras

	def get_l_frag_lines(self):
		return self.__l_frag_lines


def read_fragfrag(self_path):
	fragfrag_fn = os.path.join(self_path, 'fragfrags.txt')
	bhdr = True
	d_fragfrag = dict()
	with open(fragfrag_fn, 'rt') as fh_in:
		l_frag_lines = []
		fn_name, l_specials, l_sm_extras = None, None, None
		for line_full in fh_in:
			line = line_full.rstrip()
			if line == '': continue
			if bhdr:
				b_sm_extras = False
				defs = line.split(',')
				fn_name = defs[0]
				def_curr = 2
				if defs[1] == 's':
					b_sm_extras = True
					def_curr = 3
				num_specials = int(defs[def_curr-1])
				l_specials, l_sm_extras = [], []
				for ispecial in range(num_specials):
					# special_type, special_name, special_using = defs[def_curr:def_curr+3]
					l_specials.append(defs[def_curr:def_curr+3])
					def_curr += 3
				if b_sm_extras:
					num_sm_extras = int(defs[def_curr])
					def_curr += 1
					for i_sm_extra in range(num_sm_extras):
						l_sm_extras.append(int(defs[def_curr], 16))
						def_curr += 1
				bhdr = False
			else:
				if line != 'FRAG_END':
					l_frag_lines.append(line)
				else:
					assert fn_name != None
					d_fragfrag[fn_name] = cl_frag_data(fn_name, l_specials, l_sm_extras, l_frag_lines)
					bhdr = True
					l_frag_lines = []
					fn_name = None
	return d_fragfrag

def main():
	assert len(sys.argv) == 5, 'usage: python gvrc.py <gvr-path> <frags-path> <wspace-path> <output>'

	self_fn, gvr_path, frags_path, wspace_path, output_fn = sys.argv
	# self_path, self_app_name = os.path.split(self_fn)
	d_fragfrag = read_fragfrag(frags_path)

	# fnames = [f for f in listdir(gvr_path) if isfile(join(gvr_path, f))]
	# gvr_fnames = []
	# for fn in fnames:
	# 	_, ext = os.path.splitext(fn)
	# 	if ext == '.gvr': gvr_fnames.append(fn)
	l_out_data = []
	# for gvr_fname in gvr_fnames:
	gvr_fname = os.path.basename(gvr_path)
	gvr_dir = os.path.dirname(gvr_path)
	base, ext = os.path.splitext(gvr_fname)
	# print('compiling', os.path.join(gvr_dir, gvr_fname), 'to', os.path.join(gvr_dir, base+'.apl'), '...')
	print('compiling', os.path.join(gvr_dir, gvr_fname), 'to', os.path.join(gvr_dir, output_fn), '...')
	for fremove in glob.glob(os.path.join(wspace_path, base + '_m*' + '.gvr')):
		print('removing', fremove)
		os.remove(fremove)
	ll_macros_orig_text = [[]]
	l_orig_text = []
	gvrc_mgr = cl_gvrc()
	lmacro_args = []; lmacros = []; dmacros = dict()
	b_load_macros = True; lb_keep_spreading = [True]
	macro_level = 0
	fh_macro_spread = open(os.path.join(wspace_path, base + '_m' + str(macro_level) + '.gvr'), 'wb')
	gvrc_mgr.read_from_file(gvr_dir, gvr_dir, gvr_fname, ll_macros_orig_text[0], b_load_macros,
							lmacro_args, lmacros, dmacros, fh_macro_spread, lb_keep_spreading)
	fh_macro_spread.close()
	macro_level += 1
	b_load_macros= False
	while lb_keep_spreading[0]:
		ll_macros_orig_text.append([])
		fh_macro_spread = open(os.path.join(wspace_path, base + '_m' + str(macro_level) + '.gvr'), 'wb')
		fname_in = base + '_m' + str(macro_level-1) + '.gvr'
		gvrc_mgr.read_from_file(gvr_dir, wspace_path, fname_in, ll_macros_orig_text[macro_level], b_load_macros,
								lmacro_args, lmacros, dmacros, fh_macro_spread, lb_keep_spreading)
		fh_macro_spread.close()
		macro_level += 1
	gvrc_mgr.read_from_file(gvr_dir, wspace_path, base + '_m' + str(macro_level-1) + '.gvr', l_orig_text,
							b_load_macros = False, lmacro_args = [], lmacros = [], dmacros = None,
							fh_macro_spread = None, lb_keep_spreading = [False])
	l_out_data.append(cl_out_data(os.path.join(gvr_dir, output_fn)))
	gvrc_mgr.process(l_orig_text, l_out_data[-1])

	# fn_apl = os.path.join(gvr_path, 'gen_fused_ops.apl')
	# fh_apl = open(fn_apl, 'wt')
	# fh_apl.write(g_apl_hdr)
	# fn_apl_h = os.path.join(gvr_path, 'gen_fused_ops.apl.h')
	# fh_apl_h = open(fn_apl_h, 'wt')
	# fn_gvml_h = os.path.join(gvr_path, 'gvrc_fused_ops.h')
	# fh_gvml_h = open(fn_gvml_h, 'wt')
	# fh_gvml_h.write(g_gvml_h_hdr)
	l_frag_piece_templates = []
	for out_data_obj in l_out_data:
		full_gvr_fn = out_data_obj.get_fname()
		# if 'match' not in full_gvr_fn: continue
		l_apl_code, apl_decl, call_decl = out_data_obj.create_fused(d_fragfrag, l_frag_piece_templates)
		out_data_obj.write_file()
		# for apl_code_item, apl_decl_item, call_decl_item in zip(l_apl_code, apl_decl, call_decl):
		# 	fh_apl.write(apl_code_item)
		# 	fh_apl_h.write(apl_decl_item)
		# 	fh_gvml_h.write(call_decl_item)
	# fh_apl.close()
	# fh_apl_h.close()
	# fh_gvml_h.write(g_gvml_h_ftr)
	# fh_gvml_h.close()
	# exit()

	print('GVRC compliler v', ver,  'completed.')


if __name__ == '__main__':
    main()
